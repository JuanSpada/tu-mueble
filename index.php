<!DOCTYPE html>
<html>

  <!-- Etiqueta gsuit -->
  <meta name="google-site-verification"
             content="<meta name="google-site-verification" content="ZyXsEsBLR4o8sh6vY15hT5EqJae34W0JYFVq_tJfAzo" />
  <!-- Anti-flicker snippet (recommended)  -->
  <style>.async-hide { opacity: 0 !important} </style>
  <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
  h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
  (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
  })(window,document.documentElement,'async-hide','dataLayer',4000,
  {'GTM-5BPL4BC':true});</script>
  <head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-138525730-1"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-138525730-1', { 'optimize_id': 'GTM-KT7B7VF'});
  </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5BPL4BC');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta http-equiv="Content-Language" content="es"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Tu Mueble Deco</title>
    <link rel="icon" type="image/png" href="img/favicon.png" sizes="15x15">
    <link rel="stylesheet" href="css/grid-gallery.min.css">
    <link rel="stylesheet" href="css/master.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Sweet Alert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
  </head>
  <body>

    <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BPL4BC"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

    <div id="gg-screen" hidden></div>
    <a href="#" class="logo">
        <img src="img/logo.png" height="90">
    </a>
    <main>
      <div class="container">
        <h1 style="margin-top:35px;" class="text-center">Tu Mueble</h1>
        <h2 class="text-center" style="font-size: 1em;">Tu medida, Tu estilo</h2>
        <a href="https://github.com/jestov/grid-gallery" class="github">
          <i class="icon ion-social-github"></i>
        </a>
        <br>
        <br>
        <p style="font-size: 1.5em" class="text-center">Somos diseñadores y fábricantes de muebles hechos a medida. Los materiales que usamos son hierro y madera de demolición, amplia variación. Nos especializamos en ambos ambientes, interior y exterior.</p>
      </div>
      <div class="main">
        <div id="galeria" class="gg-box">
        <img src="img/1.jpg">
            <img src="img/2.jpg">
            <img src="img/3.jpg">
            <img src="img/4.jpg">
            <img src="img/5.jpg">
            <img src="img/6.jpg">
            <img src="img/7.jpg">
            <img src="img/8.jpg">
            <img src="img/9.jpg">
            <img src="img/10.jpg">
            <img src="img/11.jpg">
            <img src="img/12.jpg">
            <img src="img/13.jpg">
            <img src="img/14.jpg">
            <img src="img/15.jpg">
            <img src="img/16.jpg">
            <img src="img/17.jpg">
            <img src="img/19.jpg">
            <img src="img/20.jpg">

            <img src="img/new/1.jpg" alt="">
            <img src="img/new/2.jpg" alt="">
            <img src="img/new/3.jpg" alt="">
            <img src="img/new/4.jpg" alt="">
            <img src="img/new/5.jpg" alt="">
            <img src="img/new/6.jpg" alt="">
            <img src="img/new/7.jpg" alt="">
            <img src="img/new/8.jpg" alt="">
            <img src="img/new/9.jpg" alt="">
            <img src="img/new/10.jpg" alt="">
            <!-- <img src="img/new/11.jpg" alt="">
            <img src="img/new/12.jpg" alt="">
            <img src="img/new/13.jpg" alt="">
            <img src="img/new/14.jpg" alt="">
            <img src="img/new/15.jpg" alt="">
            <img src="img/new/16.jpg" alt="">
            <img src="img/new/17.jpg" alt="">
            <img src="img/new/18.jpg" alt="">
            <img src="img/new/19.jpg" alt="">
            <img src="img/new/20.jpg" alt="">
            <img src="img/new/21.jpg" alt="">
            <img src="img/new/23.jpg" alt="">
            <img src="img/new/24.jpg" alt="">
            <img src="img/new/25.jpg" alt="">
            <img src="img/new/26.jpg" alt="">
            <img src="img/new/27.jpg" alt="">
            <img src="img/new/28.jpg" alt="">
            <img src="img/new/29.jpg" alt="">
            <img src="img/new/30.jpg" alt="">
            <img src="img/new/31.jpg" alt="">
            <img src="img/new/32.jpg" alt="">
            <img src="img/new/33.jpg" alt="">
            <img src="img/new/34.jpg" alt="">
            <img src="img/new/35.jpg" alt="">
            <img src="img/new/36.jpg" alt="">
            <img src="img/new/37.jpg" alt="">
            <img src="img/new/38.jpg" alt=""> -->
        </div>
        <!-- <button id="load-more" class="btn btn-success">Cargar Más Fotos</button> -->
      </div>
      <div>
        <h3 class="text-center mt-5 mb-5">Completa el formulario para descargar el catálogo:</h3>
        <form id="contact-form" method="POST" class="col-10" style="margin: 0 auto;" action="contact.php">
          <input type="hidden" name="utm_source" value="<?= (isset($_GET['utm_source'])) ? $_GET['utm_source'] : '' ?>">
          <input type="hidden" name="utm_term" value="<?= (isset($_GET['utm_term'])) ? $_GET['utm_term'] : '' ?>">
          <input type="hidden" name="utm_campaign" value="<?= (isset($_GET['utm_campaign'])) ? $_GET['utm_campaign'] : '' ?>">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="name">Nombre</label>
                <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" required>
              </div>
              <div class="form-group col-md-6">
                <label for="phone">Teléfono</label>
                <input name="phone" type="number" class="form-control" id="phone" placeholder="Teléfono" required>
              </div>
            </div>
            <div class="form-group">
              <label for="email">Email:</label>
              <input name="email" type="email" class="form-control" id="email" placeholder="Email" required>
            </div>
            <div class="form-gorup">
                <label for="message">Consulta</label>
                <textarea name="message" id="message" class="mb-3 form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
            </div>
            <button type="submit" class="btn btn-primary mb-5">Descargar</button>
          </form>
          <div>
            <p class="text-center mb-1" style="font-weight: bold;">Contacto:</p>
            <p class="text-center mb-1">Teléfono: +54 9 11 3027-1991</p>
            <p class="text-center mb-1">Email: ventas@tumueble.com.ar</p>
            <p class="text-center mb-1">Ubicación: Villa de Mayo, Buenos Aires. Cita Previa.</p>
            <p class="text-center mb-3" style="font-style: italic">Por el momento no tenemos local a la calle pero pueden acercarse al taller coordinando una cita previa.</p>
            <div class="d-flex justify-content-center">
              <a style="margin: 5px; color: inherit;" href="https://www.facebook.com/tumuebledeco/"><i style="font-size: 2em;" class="fab fa-facebook-f"></i></a>
              <a style="margin: 5px; color: inherit;" href="https://www.instagram.com/tumuebledeco/"><i style="font-size: 2em;" class="fab fa-instagram"></i></a>
            </div>
          </div>
      </div>
      <footer>
       <p class="mr-5 pr-5">Powered by <a href="https://www.wemanagement.com.ar">We Management</a></p> 
      </footer>
    </main>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <a id="buttonWhatsapp" href="https://api.whatsapp.com/send?phone=5491130271991&text=Hola%20quiero%20más%20información." class="float" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
    </a>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/grid-gallery.min.js"></script>
    <!-- Contact from script -->
    <script src="js/contact.js"></script>
    <script src="js/imggallery.js"></script>
  </body>
</html>
